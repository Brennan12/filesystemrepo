/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "flash_driver.h"
#include "lfs.h"
#include "lfs_util.h"
#include "assert.h"
#include <stdarg.h>
#include "littlefs_api.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
void LFS_Config(void);
void command_processing(uint8_t *command, uint16_t commandLength);
int uart_tx_debug_str(const char* format, ...);
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
extern uint8_t retUSER; /* Return value for USER */
extern char USERPath[4]; /* USER logical drive path */
extern FATFS USERFatFS; /* File system object for USER logical drive */
extern FIL USERFile; /* File object for USER */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */


uint8_t lfs_read_buf[256];
uint8_t lfs_prog_buf[256];
uint8_t lfs_lookahead_buf[16];	// 128/8=16
uint8_t lfs_file_buf[256];
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


#define PAGE_SIZE 256
#define SECTOR_SIZE 4096

uint8_t debugBufferData [1024];
uint16_t debugBufferDataLength = 0;

uint8_t debugBuffer [10];


// variables used by the filesystem
lfs_t lfs;
lfs_file_t file;
static lfs_file_t _currentfile;

typedef enum
{
	CURRENT_LOG_FILE					= 1, //Keep track of the current file that logs will be written to
	CURRENT_SENDING_LOG_FILE			= 2, //Keep track of the current file that needs to be transmitted
	CURRENT_SENDING_LOG_FILE_INDEX		= 3, //Keep track of the index in die current file that needs to be transmitted
	CURRENT_SENDING_LOG_FILE_INDEX_SENT = 4, //Keep track of the index in the current file that has been sent already
	LOG_FILE_SENT						= 5, //Mark a file as sent successfully
}META_DATA;

// configuration of the filesystem is provided by this struct
struct lfs_config cfg;
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_FATFS_Init();
  MX_SPI2_Init();
  /* USER CODE BEGIN 2 */
  HAL_GPIO_WritePin(SPI_nCS_GPIO_Port, SPI_nCS_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(SPI_nHold_GPIO_Port, SPI_nHold_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(SPI_nWP_GPIO_Port, SPI_nWP_Pin, GPIO_PIN_RESET);

  HAL_UART_Receive_IT(&huart2, debugBuffer, 1);
  global_protection_unlock();
  LFS_Config();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  if (debugBufferDataLength>0)
	  {
		  if (debugBufferData[debugBufferDataLength-1] == '\n')
		  {
			  command_processing(&debugBufferData[3], debugBufferDataLength-4);
			  memset(debugBufferData,0,debugBufferDataLength);
			  debugBufferDataLength=0;
		  }

	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */


  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
typedef enum
{
	STATUS_OK				= 0,
	STATUS_ERROR			= 1,
	STATUS_TIMEOUT			= 2
} STATUS;
static char _fileConfig [] = "Config";
static char _fileBleFirmware [] = "BleFW";
static char _fileLogs[5] = "1"; //File name is just incremented, max 99999 files theoretically
static char _fileLogsHeader [] = "LogsHeader";
//as well as the index inside that file
static lfs_file_t _currentfile;
STATUS storage_create_fs();
BYTE work[512]; /* Work area (larger is better for processing time) */

#define MAX_FILE_SIZE 500
#define MAX_NUMBER_OF_BLOCKS 4 //Replaced by MAX_NUMBER_OF_FILES
#define MAX_NUMBER_OF_FILES 3
#define MAX_LOG_BUFFER_SIZE 200
static char _currentFile [] = "1" ;

void command_processing(uint8_t *command, uint16_t commandLength)
{
	//Check comms
	if (memcmp(command, (uint8_t *)"ECHO",4) == 0)
	{
	  uart_tx_debug_str("ECHO\n");
	}
	else
	//Erase chip
	if (memcmp(command, (uint8_t *)"EEPROM_ERASE",12) == 0)
	{
		chip_erase();
		uart_tx_debug_str("Flash erased\n");
	}
	else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_MOUNT",15) == 0)
	{
		LFS_Config();
		int err = lfs_mount(&lfs, &cfg);
		if (err == 0)
		{
			uart_tx_debug_str("Mount complete\n");

		}else
		{
			uart_tx_debug_str("Mount failed\n");
		}
	}
	else
	//Create file system if one is not present and mount
	if (memcmp(command, (uint8_t *)"EEPROM_FS_CREATE",16) == 0)
	{
		LFS_Config();
		uart_tx_debug_str("Creating...\n");
		int createError = lfs_format(&lfs, &cfg);

		if (createError == 0)
		{
			uart_tx_debug_str("File system created\n");
		}
		else
		{
			uart_tx_debug_str("File system error\n");
		}
	}
	else
		//Create file system if one is not present and mount
		if (memcmp(command, (uint8_t *)"EEPROM_FS_SIZE",14) == 0)
		{
			int fsSize = lfs_fs_size(&lfs);

			if (fsSize > 0)
			{
				uart_tx_debug_str("FS size: %d\n", fsSize);
			}
			else
			{
				uart_tx_debug_str("FS size error\n");
			}

		}
		else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_FILE_CREATE",21) == 0)
	{
		int fileCreate = lfs_file_open(&lfs, &file, _currentFile, LFS_O_RDWR | LFS_O_CREAT);
		if (fileCreate == 0)
		{
			uart_tx_debug_str("File created + opened\n");
		}
		else
		{
			uart_tx_debug_str("Error opening file - create\n");
		}
	}

	else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_FILE_OPEN_WRITE",25) == 0)
	{
		int fileOpenWrite = lfs_file_open(&lfs, &file, _currentFile, LFS_O_APPEND);
		if (fileOpenWrite == 0)
		{
			uart_tx_debug_str("File Opened - Write mode\n");
		}
		else
		{
			uart_tx_debug_str("Error opening file - write\n");
		}
	}
	else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_FILE_OPEN_READ",24) == 0)
	{
		int fileOpenRead = lfs_file_open(&lfs, &file, _currentFile, LFS_O_RDONLY);
		if (fileOpenRead == 0)
		{
			uart_tx_debug_str("File opened - read mode\n");
		}
		else
		{
			uart_tx_debug_str("Error opening file - read\n");
		}
	}

	else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_FILE_CLOSE",20) == 0)
	{

		int err = lfs_file_close(&lfs, &file);

		if (err == 0)
		{
			uart_tx_debug_str("File closed\n");
		}else
		{
			uart_tx_debug_str("File closed failed\n");
		}
	}
	else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_GET_FILE_SIZE",23) == 0)
	{
		int fileSize = lfs_file_size(&lfs, &file);
		uart_tx_debug_str("File size: %d\n",fileSize);
	}
	else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_FILE_WRITE",20) == 0)
	{
		uint8_t data [] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor"
				" incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation "
				"ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in"
				" voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non "
				"proident, sunt in culpa qui officia deserunt mollit anim id est laborum $";

		if (lfs_file_size(&lfs, &file) < MAX_FILE_SIZE)
		{
			//There is space in this file, or it should be the last log in this file

		}
		int writeStatus = lfs_file_write(&lfs, &file, data, sizeof(data));

		if (writeStatus > 0)
		{
			uart_tx_debug_str("Wrote %d bytes\n", writeStatus);
		}else
		{
			uart_tx_debug_str("File write failed\n");
		}
	}
	else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_FILE_READ",19) == 0)
	{
		//First written data is always present. Breaking with multiple writes
		uint8_t data [256] = {0};

		int readResult = lfs_file_read(&lfs, &file, data, sizeof(data));

		if (readResult > 0)
		{
			uart_tx_debug_str("Read %d bytes\n", readResult);
		}
		else
		{
			uart_tx_debug_str("File read failed\n");
		}
	}

	//Not working
	else
	if (memcmp(command, (uint8_t *)"EEPROM_FS_FILE_DUMP",19) == 0)
	{
		int fileSize = lfs_file_size(&lfs, &file);

		uint8_t data [256] = {0};

		while (fileSize > 0)
		{
			int readResult = lfs_file_read(&lfs, &file, data, sizeof(data));
			uart_tx_debug_str((char *)data);
			uart_tx_debug_str("\n");
			fileSize -= readResult;
			HAL_Delay(500);
		}
	}
	else
	if (memcmp(command, (uint8_t *)"INITIAL_CREATE",14) == 0)
	{
//		if (lfs_mount(&lfs, &cfg) != 0)
//		{
			storage_create_fs();
//		}


	}
	else
	if (memcmp(command, (uint8_t *)"STORE_LOGS",10) == 0)
	{
		//HAL_Delay(500);
		int err = 0;
		uint8_t data2 [] = {
				//Multiple log structure
				0,35, 					//Total size
				0,1, 					//Number of logs in the file
				2,						//Device type
				1,2,3,4,5,6,7,8,		//Device UUID
				0x01,0x07,0,0,			//Sensor structures

				//Single log structure
				19,						//Log size
				0,12,15,14,13,12,0,2, 	//Time-stamp
				8,						//Event type (BLE beacon change)
				8,8,8,8,8,8,8,8,		//Beacon change ID
				0,27					//Beacon battery level
		};

		//Check what the current file is - Check meta data on the header file
		char currentFileMeta [5] = {0};
		err += lfs_getattr(&lfs, _fileLogsHeader, CURRENT_LOG_FILE, currentFileMeta, 5);

		char sentBuffer [5] = {0};
		lfs_getattr(&lfs, currentFileMeta, LOG_FILE_SENT, sentBuffer, 1);
		int fileIsSent = atoi(sentBuffer);

		if (fileIsSent == 2)
		{
			int nextFile = atoi(currentFileMeta);
			nextFile++;

			if (nextFile > MAX_NUMBER_OF_FILES)
			{

				err += lfs_setattr(&lfs, _fileLogsHeader, CURRENT_LOG_FILE, "1", 1);
				//Create or clear the next file or reset to the first file

				err += lfs_remove(&lfs, "1"); //may break if the file does not exist (TBD)
				err += lfs_file_open(&lfs, &_currentfile, "1", LFS_O_CREAT);
				err += lfs_file_close(&lfs, &_currentfile);
				lfs_setattr(&lfs, "1", LOG_FILE_SENT, "1", 1);//Mark the file as unsent
				memset(_fileLogs,0,sizeof(_fileLogs));
				_fileLogs[0] = '1';
				uart_tx_debug_str("Moving to first file - Last is sent\n");
			}
			else
			{
				//Write back the next file number
				int nextFileNumberLength1 = sprintf(_fileLogs,"%d",nextFile);
				err += lfs_setattr(&lfs, _fileLogsHeader, CURRENT_LOG_FILE, _fileLogs, nextFileNumberLength1);

				//Create or clear the next file or reset to the first file

				err += lfs_remove(&lfs, _fileLogs); //may break if the file does not exist (TBD)
				err += lfs_file_open(&lfs, &_currentfile, _fileLogs, LFS_O_CREAT);
				err += lfs_file_close(&lfs, &_currentfile);
				lfs_setattr(&lfs, _fileLogs, LOG_FILE_SENT, "1", 1);//Mark the file as unsent
				uart_tx_debug_str("Moving to next file - Previous is sent\n");
			}

		}
		else
		{
			memcpy(_fileLogs,currentFileMeta,sizeof(currentFileMeta));
		}


		int currentFile = atoi(_fileLogs);
		//Open the current log file
		err = lfs_file_open(&lfs, &_currentfile, _fileLogs, LFS_O_APPEND);

		uart_tx_debug_str("Current file: %s\n", _fileLogs);
		uart_tx_debug_str("Wrote %d bytes\n",lfs_file_write(&lfs, &_currentfile, data2, sizeof(data2)));
		uart_tx_debug_str("New file size: %d\n",lfs_file_size(&lfs, &_currentfile));


		//If the file size exceeds to max file size, create/re-create the next file and set current log file to that file

		if ((lfs_file_size(&lfs, &_currentfile) >= MAX_FILE_SIZE))
		{
			lfs_file_close(&lfs, &_currentfile);//We don't need the file anymore
			if (currentFile >= MAX_NUMBER_OF_FILES)
			{
				//Reset to first file
				err += lfs_setattr(&lfs, _fileLogsHeader, CURRENT_LOG_FILE, "1", 1);

				//Clear first file
				err += lfs_remove(&lfs, (char *)"1");
				err += lfs_file_open(&lfs, &_currentfile, (char *)"1", LFS_O_CREAT);
				err += lfs_file_close(&lfs, &_currentfile);
				lfs_setattr(&lfs, "1", LOG_FILE_SENT, "1", 1);//Mark the file as unsent
				//reset _fileLogs to the first file
				memset(_fileLogs,0,sizeof(_fileLogs));
				_fileLogs[0] = '1';
				uart_tx_debug_str("Resetting to file 1\n");
			}
			else
			{
				//Proceed to next file and create/clear that file

				//Increment the working file
				int nextFileNumber = currentFile;
				nextFileNumber++;

				//Write back the next file number
				int nextFileNumberLength = sprintf(_fileLogs,"%d",nextFileNumber);
				err += lfs_setattr(&lfs, _fileLogsHeader, CURRENT_LOG_FILE, _fileLogs, nextFileNumberLength);

				//Create or clear the next file
				err += lfs_remove(&lfs, _fileLogs); //may break if the file does not exist (TBD)
				err += lfs_file_open(&lfs, &_currentfile, _fileLogs, LFS_O_CREAT);
				err += lfs_file_close(&lfs, &_currentfile);
				lfs_setattr(&lfs, _fileLogs, LOG_FILE_SENT, "1", 1);//Mark the file as unsent
				uart_tx_debug_str("Moving to next file\n");
				//uart_tx_debug_str("Blocks used: %d\n", lfs_fs_size(&lfs));
			}

			return; // so we don't close an already closed file
		}
		lfs_file_close(&lfs, &_currentfile);

	}
	else
	if (memcmp(command, (uint8_t *)"GET_LOG",7) == 0)
	{
		int err2 = 0;
		//Get current file that still needs to be logged
		char currentLogSendFile [5] = {0};
		err2 += lfs_getattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE, currentLogSendFile, 5);
		int icurrentLogSendFile = atoi(currentLogSendFile);
		uart_tx_debug_str("Current send log: %d\n", icurrentLogSendFile);

		//Check if the file has already been sent
		char hasBeenSent [1] = {0};
		lfs_getattr(&lfs,currentLogSendFile, LOG_FILE_SENT, hasBeenSent, 1);
		if (atoi(hasBeenSent) == 2)
		{
			uart_tx_debug_str("File has already been sent. No new logs\n");
			return;
		}

		//Seek to the current index of the file
		char cSeekIndex [5] = {0};
		err2 += lfs_getattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX_SENT, cSeekIndex, 5);
		int icurrentLogSendFileIndex = atoi((char *)cSeekIndex);

		err2 = lfs_file_open(&lfs, &_currentfile, (char *)currentLogSendFile, LFS_O_RDONLY);
		if (err2 != 0)
		{
			uart_tx_debug_str("File does not exist yet\n");
			return;
		}
		err2 += lfs_file_seek(&lfs, &_currentfile, icurrentLogSendFileIndex, LFS_SEEK_SET);

		uint8_t content [1024] = {0};
		uint16_t contentCounter = 0;
		int readResult = 0xFF;
		int headerSize = 0;

		while (1)
		{
			//Get the log header (17 bytes)
			if (contentCounter + 17 > MAX_LOG_BUFFER_SIZE) break; //keep log size below 1024
			headerSize = lfs_file_read(&lfs, &_currentfile, &content[contentCounter], 17);
			if (headerSize == 0 || headerSize != 17) break;
			contentCounter += headerSize;

			//Read out the message based on the length from the header
			uint16_t logPackLength = (uint16_t)((uint16_t) (content[contentCounter-17] << 8) | content[contentCounter-16] ) + 2 - 17;
			if (contentCounter +logPackLength > MAX_LOG_BUFFER_SIZE) //Keep log size below 1024
			{
				 //Remove the last 17 bytes and don't add the rest of the log
				 memset(&content[contentCounter-17],0,17);
				 contentCounter -= 17;
				 break;
			}
			readResult = lfs_file_read(&lfs, &_currentfile, &content[contentCounter], logPackLength);
			contentCounter += readResult;
		}


		char cContentCounter [5] = {0};
		sprintf(cContentCounter,"%d",contentCounter);

		uart_tx_debug_str("Total send log size: %d\n",contentCounter);
		lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX, cContentCounter, 5);
		lfs_file_close(&lfs, &_currentfile);

	}
	else
	if (memcmp(command, (uint8_t *)"LOG_SENT",8) == 0)
	{
		//The log has been sent, we can now move the pointer in a file, or move to the next file and mark the file as sent

		//Get the current file and its size
		char cCurrentFile [5] = {0};
		lfs_getattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE, cCurrentFile, 5);
		int iCurrentFile = atoi(cCurrentFile);
		uart_tx_debug_str("Current send log: %d\n", iCurrentFile);

		char cCurrentFileIndex [5] = {0};
		lfs_getattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX, cCurrentFileIndex, 5);
		int iCurrentFileIndex = atoi(cCurrentFileIndex);

		char cCurrentFileIndexSent [5] = {0};
		lfs_getattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX_SENT, cCurrentFileIndexSent, 5);
		int iCurrentFileIndexSent = atoi(cCurrentFileIndexSent);

		iCurrentFileIndex += iCurrentFileIndexSent;
		//Check if the index covers the entire file, if it does, move to the next file and mark this one as sent
		int fileExists = lfs_file_open(&lfs, &_currentfile, cCurrentFile, LFS_O_RDONLY);
		int fileSize = 0;
		if (fileExists != 0)
		{
			uart_tx_debug_str("File does not exist yet\n");
			return;
		}
	    fileSize = lfs_file_size(&lfs, &_currentfile);
		lfs_file_close(&lfs, &_currentfile);

		if (fileSize == 0)
		{
			uart_tx_debug_str("File is empty, cannot send any logs\n");
			return;
		}

		if (fileSize > iCurrentFileIndex)
		{
			//The entire file has not been sent, update the current send index
			uart_tx_debug_str("Sent %d bytes\n",iCurrentFileIndex-iCurrentFileIndexSent);
			//lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX, cCurrentFileIndex, 5);
			sprintf(cCurrentFileIndex, "%d",iCurrentFileIndex);
			lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX_SENT, cCurrentFileIndex, 5);
		}else
		{
			//Mark current file as sent
			lfs_setattr(&lfs, cCurrentFile, LOG_FILE_SENT, "2", 1);
			uart_tx_debug_str("Sent %d bytes\n",iCurrentFileIndex-iCurrentFileIndexSent);
			//Move to the next file
			sprintf(cCurrentFile, "%d", ++iCurrentFile );

			if (iCurrentFile > MAX_NUMBER_OF_FILES)
			{
				//Reset to start logging from the first file again
				lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE, "1", 1);
				uart_tx_debug_str("Resetting to first file\n");

			}else
			{
				lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE, cCurrentFile, 5);
				uart_tx_debug_str("Moving to next file to be sent\n");

			}
			lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX, "0", 1);
			lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX_SENT, "0", 1);
		}

	}
	else
	if (memcmp(command, (uint8_t *)"ADD_ATTRIBUTE",13) == 0)
	{
		uint8_t sentStatus [] = {0x23};
		int addAttr = lfs_setattr(&lfs, _fileLogs, CURRENT_LOG_FILE, sentStatus, sizeof(sentStatus));

		if (addAttr == 0)
			uart_tx_debug_str("Added attribute\n");
		else
			uart_tx_debug_str("Added attribute failed\n");

	}
	else
	if (memcmp(command, (uint8_t *)"GET_ATTRIBUTE",13) == 0)
	{
		uint8_t sentStatusGet [1] = {0x00};

		int getAttr = lfs_getattr(&lfs, _fileLogs, CURRENT_LOG_FILE, sentStatusGet, sizeof(sentStatusGet));

		if (getAttr > 0 )
			uart_tx_debug_str("Get attribute success\n");
		else
			uart_tx_debug_str("Get attribute failed\n");
	}
	else
	if (memcmp(command, (uint8_t *)"STORE_FIRMWARE\n",15) == 0)
	{
		uint8_t firmwareFile [1024];
		memset(firmwareFile,'A',1024);

		//Clear all other files

		int test = lfs_file_open(&lfs, &_currentfile, _fileBleFirmware, LFS_O_CREAT | LFS_O_APPEND);
		if (test == 0) uart_tx_debug_str("Firmware file created\n");

		for (int i=0;i<5;i++)
		{
			lfs_file_write(&lfs, &_currentfile, firmwareFile, sizeof(firmwareFile));
		}
		uart_tx_debug_str("Firmware file stored\n");
		lfs_file_close(&lfs, &_currentfile);
		int test2 = lfs_file_open(&lfs, &_currentfile, _fileBleFirmware, LFS_O_RDONLY);
		if (test2 == 0 )uart_tx_debug_str("Firmware opened in read mode\n");
	}
	else
	if (memcmp(command, (uint8_t *)"GET_FIRMWARE\n",13) == 0)
	{
		uint8_t firmwareFile [1024] = {'A'};

		//Clear all other files

		//int test = lfs_file_open(&lfs, &_currentfile, _fileBleFirmware, LFS_O_RDONLY);
		//if (test == 0) uart_tx_debug_str("Firmware file opened\n");

		int readLength = 0xFF;

		readLength = lfs_file_read(&lfs, &_currentfile, firmwareFile, sizeof(firmwareFile));

		uart_tx_debug_str("Firmware file read: %d bytes\n", readLength);
		if (readLength == 0) lfs_file_close(&lfs, &_currentfile);
	}
	else
	{
		uart_tx_debug_str("Invalid Command\n");
	}
}

#define FSCS(x) if(x != 0) return STATUS_ERROR

STATUS storage_create_fs()
{
	//Format the storage before creating the file system
	chip_erase();
	int err = lfs_format(&lfs, &cfg);
	if (err == 0)
	{
		int test1 = lfs_mount(&lfs, &cfg);
		if (test1 == 0) uart_tx_debug_str("Mounted\n");
		//Create a config file
		int test2 = lfs_file_open(&lfs, &_currentfile, _fileConfig, LFS_O_CREAT);
		if (test2 == 0) uart_tx_debug_str("Config file created\n");

		int test3 = lfs_file_close(&lfs, &_currentfile);
		if (test3 == 0) uart_tx_debug_str("Config file closed\n");

		//Create a log counter file and write a 1 in.
		int test4 = lfs_file_open(&lfs, &_currentfile, _fileLogsHeader, LFS_O_CREAT | LFS_O_WRONLY);
		if (test4 == 0) uart_tx_debug_str("Log header created\n");

		/*Add meta data to the header file*/
		int test5 = lfs_setattr(&lfs, _fileLogsHeader, CURRENT_LOG_FILE, "1", 1);
		test5 += lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE, "1", 1);
		test5 += lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX, "0", 1);
		test5 += lfs_setattr(&lfs, _fileLogsHeader, CURRENT_SENDING_LOG_FILE_INDEX_SENT, "0", 1);
		if (test5 == 0) uart_tx_debug_str("Set meta data on header file\n");

		int test6 = lfs_file_write(&lfs, &_currentfile, (char *)"1", 1); // Will always start as 1 here
		if (test6 == 0) uart_tx_debug_str("Wrote %d bytes\n",test6);

		int test7 = lfs_file_close(&lfs, &_currentfile);
		if (test7 == 0) uart_tx_debug_str("Log header closed \n");

		//Create a first log file
		int test8 = lfs_file_open(&lfs, &_currentfile, _fileLogs, LFS_O_CREAT);
		if (test8 == 0) uart_tx_debug_str("First log file created\n");

		int test9 = lfs_file_close(&lfs, &_currentfile);
		if (test9 == 0) uart_tx_debug_str("First log file closed\n");

		return STATUS_OK;
	}
	return STATUS_ERROR;
}
int uart_tx_debug_str(const char* format, ...)
{
	char buf[1024];
	va_list argptr;
	va_start(argptr, format);
	vsnprintf(buf, 1024, format, argptr);
	va_end(argptr);

	/*DEBUG UART*/
	HAL_UART_Transmit(&huart2,(uint8_t *)buf, strlen(buf), 50);
	return 0;
}


int block_device_read(const struct lfs_config *c, lfs_block_t block,lfs_off_t off, void *buffer, lfs_size_t size);
int block_device_prog(const struct lfs_config *c, lfs_block_t block,lfs_off_t off, const void *buffer, lfs_size_t size);
int block_device_erase(const struct lfs_config *c, lfs_block_t block);
int block_device_sync(const struct lfs_config *c);


#define LFS_READ_SIZE 		256
#define LFS_PROG_SIZE 		256
#define LFS_BLOCK_SIZE 		4096
#define LFS_BLOCK_COUNT 	2000
#define LFS_LOOKAHEAD_SIZE  16
#define LFS_BLOCK_CYCLES 	500
#define LFS_CACHE_SIZE 		256
#define LFS_MAX_FILE_SIZE	3900 //? TBD
void LFS_Config(void)
{

	// block device operations
	cfg.read  = lfs_read;
	cfg.prog  = lfs_prog;
	cfg.erase = lfs_erase;
	cfg.sync  = lfs_sync;

	// block device configuration
	cfg.read_size = LFS_READ_SIZE;
	cfg.prog_size = LFS_PROG_SIZE;
	cfg.block_size = LFS_BLOCK_SIZE;
	cfg.block_count = LFS_BLOCK_COUNT;
	cfg.lookahead_size = LFS_LOOKAHEAD_SIZE;
	cfg.block_cycles = LFS_BLOCK_CYCLES;
	cfg.cache_size = LFS_CACHE_SIZE;

}
int block_device_read(const struct lfs_config *c, lfs_block_t block,lfs_off_t off, void *buffer, lfs_size_t size)
{
    assert(off  % c->read_size == 0);
    assert(size % c->read_size == 0);
    assert(block < c->block_count);



    uint32_t addr = block*4096 + off;
    read_data(READ, addr, size, buffer);

	return 0x00;
}

int block_device_prog(const struct lfs_config *c, lfs_block_t block,lfs_off_t off, const void *buffer, lfs_size_t size)
{
    assert(off  % c->read_size == 0);
    assert(size % c->read_size == 0);
    assert(block < c->block_count);

    uint32_t addr = block*4096 + off;
    write_data(PP, addr, size, buffer);
	return 0x00;
}

int block_device_erase(const struct lfs_config *c, lfs_block_t block)
{
	assert(block < c->block_count);

    int block_addr = block*4096;

    sector_erase(block_addr);


	return 0x00;
}

int block_device_sync(const struct lfs_config *c)
{
	return 0x00;
}


/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
