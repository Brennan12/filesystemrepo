/*
 * littlefs_api.c
 *
 *  Created on: Aug 10, 2020
 *      Author: Brennan
 */


#include "littlefs_api.h"

// Read a region in a block. Negative error codes are propogated
// to the user.
int lfs_read(const struct lfs_config *c, lfs_block_t block,lfs_off_t off, void *buffer, lfs_size_t size)
{
	uint32_t address = (block*4096)+off;

	read_data(READ, address, size, buffer);

	return 0;
}

int lfs_prog(const struct lfs_config *c, lfs_block_t block,lfs_off_t off, const void *buffer, lfs_size_t size)
{
	uint32_t address = (block*4096)+off;

	write_data(PP, address, size, buffer);
	return 0;
}

int lfs_erase(const struct lfs_config *c, lfs_block_t block)
{
	uint32_t address = (block*4096);

	sector_erase(address);
	return 0;
}

int lfs_sync(const struct lfs_config *c)
{
	return 0;
}

