/*
 * flash_driver.c
 *
 *  Created on: 21 May 2020
 *      Author: brennan
 */


#include "flash_driver.h"

static volatile DSTATUS Stat = STA_NOINIT;	/* Disk Status */

DSTATUS flash_status(BYTE drv)
{
	return 0x00;
//	if (drv) return STA_NOINIT;
//	return Stat;
}

DRESULT flash_read(BYTE pdrv, BYTE* buff, DWORD sector, UINT count)
{
	//if (Stat & STA_NOINIT) return RES_NOTRDY;

	uint32_t address = sector*512;

	if (count == 1)
	{
		read_data(READ, address, 512, buff);
		count = 0;
	}else
	{
		do {
			read_data(READ, address, 512, buff);
			address += 512;
			buff += 512;
		} while (--count);
	}

	return count ? RES_ERROR : RES_OK;
}

DRESULT flash_write(BYTE pdrv, const BYTE* buff, DWORD sector, UINT count)
{
	//All data before new data to be written is erased. This means we have to read and hold all previous data before
	//erasing a sector

	//if (Stat & STA_NOINIT) return RES_NOTRDY;
	global_protection_unlock();

	uint32_t address = sector*512;

	if (count == 1)
	{
		sector_erase(address);
		write_data(PP, address, 256, buff);
		address += 256;
		buff += 256;
		write_data(PP, address, 256, buff);
		count = 0;
	}else
	{
		do {
			sector_erase(address);
			write_data(PP, address, 256, buff);
			address += 256;
			buff += 256;
			write_data(PP, address, 256, buff);
			address += 256;
			buff += 256;
		}while (--count);
	}

	return count ? RES_ERROR : RES_OK;
}


DRESULT flash_ioctl(BYTE drv, BYTE ctrl, void *buff)
{
	//if (Stat & STA_NOINIT) return RES_NOTRDY;
	DRESULT res;
	switch (ctrl)
			{
			case GET_SECTOR_COUNT:
				/* SEND_CSD */
				*(WORD*) buff = 8192;
				res = RES_OK;
				break;
			case GET_SECTOR_SIZE:
				*(WORD*) buff = 512;
				res = RES_OK;
				break;
			case CTRL_SYNC:
				res = RES_OK;
				break;
			case GET_BLOCK_SIZE:
				*(WORD*) buff = 4096;
				res = RES_OK;

			default:
				res = RES_PARERR;
			}

		return res;

}
void read_data(uint16_t cmd, uint32_t address, uint16_t count, uint8_t *data)
{

	  uint8_t command [1024] = {0} ;
	  uint8_t commandLength;

	  switch (cmd)
	  {
	  case ID:
		  commandLength = 1;
		  command[0] = cmd;
		  break;
	  case READ:
		  commandLength = 4;
		  command[0] = cmd;
		  command[3] = (address & 0x000000ff);
		  command[2] = (address & 0x0000ff00) >> 8;
		  command[1] = (address & 0x00ff0000) >> 16;
		  break;
	  }

	  reset_cs();

	  HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	  HAL_Delay(20);
	  HAL_SPI_Receive(&hspi2, data, count, 50);

	  set_cs();
}

void write_data(uint16_t cmd, uint32_t address, uint16_t count, const uint8_t *data)
{
	  //read out entire sector before writing over it.
	  uint8_t command [1024] = {0};
	  uint16_t commandLength = 0;

	  write_enable();
	  reset_cs();
	  commandLength = count+4;
	  command[0] = cmd;
	  command[3] = (address & 0x000000ff);
	  command[2] = (address & 0x0000ff00) >> 8;
	  command[1] = (address & 0x00ff0000) >> 16;

	  memcpy(&command[4],data,count);
	  HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	  HAL_Delay(30);

	  set_cs();
	  write_disable();
}

void sector_erase(uint32_t address)
{
	uint8_t command [10] = {0x00};
	uint8_t commandLength ;

	commandLength = 4;
	command[0] = SE;
	command[1] =  (uint8_t)((address >> 16) & 0xFF);
	command[2] =  (uint8_t)((address >> 8) & 0xFF);
	command[3] =  (uint8_t)((address >> 0) & 0xFF);

	write_enable();

	reset_cs();
	HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	set_cs();

	write_disable();
}

void block_erase(uint32_t address)
{
	uint8_t command [10] = {0x00};
	uint8_t commandLength ;

	commandLength = 4;
	command[0] = BE;
	command[1] =  (uint8_t)((address >> 16) & 0xFF);
	command[2] =  (uint8_t)((address >> 8) & 0xFF);
	command[3] =  (uint8_t)((address >> 0) & 0xFF);

	write_enable();

	reset_cs();
	HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	set_cs();

	write_disable();
}

void chip_erase()
{
	uint8_t command [10] = {0x00};
	uint8_t commandLength ;

	commandLength = 1;
	command[0] = CE;

	write_enable();

	reset_cs();
	HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	set_cs();

	write_disable();
}

void global_protection_unlock()
{
	uint8_t command [10] = {0x00};
	uint8_t commandLength ;

	commandLength = 1;
	command[0] = ULBPR;

	write_enable();

	reset_cs();
	HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	set_cs();

	write_disable();
}

void write_status_register()
{
	uint8_t command [1024] = {WRSR};
	uint8_t commandLength = 1;

	write_enable();

	commandLength = 2;
	command[0] = PP;
	command[1] = 0x00;

	reset_cs();

	HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	set_cs();

	write_disable();
}

void set_cs()
{
	HAL_GPIO_WritePin(SPI_nCS_GPIO_Port, SPI_nCS_Pin, GPIO_PIN_SET);
	HAL_Delay(5);
}

void reset_cs()
{
	HAL_GPIO_WritePin(SPI_nCS_GPIO_Port, SPI_nCS_Pin, GPIO_PIN_RESET);
	HAL_Delay(5);
}



void write_enable()
{
	  uint8_t command [10] = {WREN};
	  uint8_t commandLength = 1;

	  reset_cs();
	  HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	  HAL_Delay(5);
	  set_cs();

}

void write_disable()
{
	  uint8_t command [1024] = {WRDI};
	  uint8_t commandLength = 1;

	  reset_cs();
	  HAL_SPI_Transmit(&hspi2,command,commandLength,50);
	  HAL_Delay(5);
	  set_cs();
}
