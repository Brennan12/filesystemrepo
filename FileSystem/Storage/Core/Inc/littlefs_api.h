/*
 * littlefs_api.h
 *
 *  Created on: Aug 10, 2020
 *      Author: Brennan
 */

#ifndef INC_LITTLEFS_API_H_
#define INC_LITTLEFS_API_H_

#include "lfs.h"
#include "flash_driver.h"

int lfs_read(const struct lfs_config *c, lfs_block_t block,lfs_off_t off, void *buffer, lfs_size_t size);
int lfs_prog(const struct lfs_config *c, lfs_block_t block,lfs_off_t off, const void *buffer, lfs_size_t size);
int lfs_sync(const struct lfs_config *c);
int lfs_erase(const struct lfs_config *c, lfs_block_t block);

#endif /* INC_LITTLEFS_API_H_ */
