/*
 * flash_driver.h
 *
 *  Created on: 21 May 2020
 *      Author: brennan
 */

#ifndef INC_FLASH_DRIVER_H_
#define INC_FLASH_DRIVER_H_

#include <stdint.h>
#include <string.h>
#include "spi.h"
#include "integer.h"
#include "diskio.h"


#define ID   0x9F
#define WREN 0x06 //Write enable
#define WRDI 0x04 //Write disable
#define READ 0x03 //Read command
#define PP	 0x02
#define SE   0x20
#define BE   0xD8
#define WRSR 0x01
#define CE   0xC7
#define ULBPR 0x98


DRESULT flash_ioctl(BYTE drv, BYTE ctrl, void *buff);
DRESULT flash_write(BYTE pdrv, const BYTE* buff, DWORD sector, UINT count);
DRESULT flash_read(BYTE pdrv, BYTE* buff, DWORD sector, UINT count);
DSTATUS flash_status(BYTE drv);

void read_data(uint16_t cmd, uint32_t address, uint16_t count, uint8_t *data);
void write_data(uint16_t cmd, uint32_t address, uint16_t count, const uint8_t *data);
void write_enable();
void write_disable();
void set_cs();
void reset_cs();
void write_status_register();
void sector_erase(uint32_t address);
void block_erase(uint32_t address);
void chip_erase();
void global_protection_unlock();

#endif /* INC_FLASH_DRIVER_H_ */
